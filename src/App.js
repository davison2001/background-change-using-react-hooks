import { useState } from "react";


const App = () => {
  const originalColor = '#8e44ad'
  const [background, setBackground] = useState(originalColor);
  const [name, setName] = useState('CLICK ME');

  const change_background = () => {

    var newBackground = '#34495e';
    setBackground(newBackground);
    setName('Clicked 😃😃😃');

  };

  const change_back = () => {
    var newBackground = 'lightBlue';
    setBackground(newBackground);
    setName('Clicked Twice 😁😁😁');
  }

  return (
    <>
      <div style={{ backgroundColor: background }}>
        <button onClick={change_background} onDoubleClick = {change_back}> {name} </button>
      </div>
    </>
  );
};

export default App;
